#ifndef FECHA_H
#define FECHA_H
#include <tiempo.h>

class Fecha
{
private:
    static const int _dlength[13];
    int _dia;
    int _mes;
    int _anio;
    bool esBisiesto(int);
public:
    Fecha()  : _dia(1), _mes(1),_anio(1900){}
    Fecha(const Fecha& F) : _dia(F._dia),_mes(F._mes),_anio(F._anio){}
    Fecha(int dia ,int mes,int anio) : _dia(dia), _mes(mes), _anio(anio) {}
    ~Fecha(){}
    inline int getDia(){return _dia;}
    inline int getMes(){return _mes;}
    inline int getAnio(){return _anio;}
    bool setDia(int);
    bool setMes(int);
    bool setAnio(int);
    bool addDias(int);
    bool addMeses(int);
    bool addAnios(int);
    Fecha& operator=(const Fecha&);
    friend ostream & operator << ( ostream &, Fecha &);

};

#endif // FECHA_H
