#ifndef TIEMPO_H
#define TIEMPO_H
#include <iostream>

using namespace  std;

class Tiempo
{
private:
    int _hora;
    int _minuto;
    int _segundo;
    bool _mode;
    static int _id;
public:
    Tiempo(): _hora(0),_minuto(0),_segundo(0) {_id++;}
    Tiempo(const Tiempo& T) : _hora(T._hora),_minuto(T._minuto), _segundo(T._segundo) {_id++;}
    Tiempo(int,int,int,bool);
    ~Tiempo(){_id--;}
    int getHora();
    inline int getMinuto(){return _minuto;}
    inline int getSegundo(){return _segundo;}
    bool setHora(int);
    bool setMinuto(int);
    bool setSegundo(int);
    bool setMode(bool);
    int addHoras(int);
    int addMinutos(int);
    Tiempo& operator=(const Tiempo&);
    Tiempo& operator++();
    friend ostream & operator << ( ostream &, Tiempo &);
};

#endif // TIEMPO_H
