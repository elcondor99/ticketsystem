#include "tiempo.h"

int Tiempo::_id = 0;

int Tiempo::getHora()
{
    if(_hora >= 13 && _mode == false)
    {
        return (_hora-12);
    }
    return _hora;
}
bool Tiempo::setHora(int h)
{
    if(h >=0 && h<=23)
    {
        _hora = h;
        return true;
    }
    else
    {
        return false;
    }
}
bool Tiempo::setMinuto(int m)
{
    if(m>=0 && m<=59)
    {
       _minuto = m;
        return true;
    }
    else
    {
        return false;
    }
}
bool Tiempo::setSegundo(int s)
{
    if(s>=0 && s<=59)
    {
        _segundo = s;
        return true;
    }
    else
    {
        return false;
    }
}
bool Tiempo::setMode(bool a)
{
    if(a == true || a == false)
    {
        _mode = a;
        return true;
    }
    else
    {
        return false;
    }
}
Tiempo::Tiempo(int h, int m, int s, bool mo)
{
    _id++;
    try
    {
       if(!(setHora(h) && setMinuto(m) && setSegundo(s) && setMode(mo)))
       {
           *this = Tiempo();
           throw 1;
       }
    }catch(...){
        cerr << "Error: Clock Setted to Default ID:" << _id << endl;
    }
}

Tiempo& Tiempo::operator =(const Tiempo& T)
{
    if(this != &T)
    {
        _hora = T._hora;
        _minuto = T._minuto;
        _segundo = T._segundo;
        _mode = T._mode;
        return *this;
    }
    else
    {
        return *this;
    }
}
Tiempo& Tiempo::operator ++()
{
    if(_segundo != 59)
    {
        _segundo++;
    }
    else
    {
        _segundo = 0;
        if(_minuto != 59)
        {
            _minuto++;
        }
        else
        {
            _minuto = 0;
            if(_hora != 23)
            {
                _hora++;
            }
            else
            {
                _hora = 0;
            }
        }
    }
    return *this;
}
ostream & operator << ( ostream & output, Tiempo & T )
{
    if(T._hora < 10)
    {
        output << "0" << T._hora << ":";
    }
    else
    {
        output << T._hora << ":";
    }
    if(T._minuto < 10)
    {
        output << "0" << T._minuto << ":";
    }
    else
    {
        output << T._minuto<< ":";
    }
    if(T._segundo < 10)
    {
        output << "0" << T._segundo;
    }
    else
    {
        output << T._segundo;
    }
    output << endl;
    return output;
}

int Tiempo::addHoras(int h)
{
    int d = 0;
    if(h<0)
    {
        return -1;
    }
    else
    {
        if(_hora + h <= 23)
        {
            _hora += h;
        }
        else
        {
            while(_hora + h > 23)
            {
                h = h - 24;
                d++;
            }
            _hora += h;
        }

    }
    return d;
}

int Tiempo::addMinutos(int m)
{
    int d = 0;
    int h = 0;
    if(m<0)
    {
        return -1;
    }
    else
    {
        if(m == 60)
        {
            d = addHoras(1);
        }
        else if(_minuto + m < 59)
        {
            _minuto = _minuto + m;
        }
        else
        {
            h = m/60;
            d = addHoras(h);
            m = m - (h*60);
            _minuto = _minuto + m;
        }
    }
    return d;
}
