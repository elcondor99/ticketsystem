TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    tiempo.cpp \
    fecha.cpp \
    avion.cpp \
    boeing747.cpp \
    asiento.cpp \
    codigovuelo.cpp \
    vuelo.cpp

HEADERS += \
    tiempo.h \
    fecha.h \
    avion.h \
    boeing747.h \
    asiento.h \
    codigovuelo.h \
    vuelo.h
