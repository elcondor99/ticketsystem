#include "boeing747.h"

void Boeing747::setAsientos()
{
    void* ptr = operator new[]( _sizeEconomica+_sizeEjecutiva * sizeof( Asiento ) );
    _asientos = static_cast<Asiento*>( ptr );
    for(int i=0;i <_sizeEconomica;i++ )
    {
        new( &_asientos[i] )Asiento(economico, i+1 );
    }
    for(int i=_sizeEconomica;i <_sizeEconomica+_sizeEjecutiva;i++ )
    {
        new( &_asientos[i] )Asiento(ejecutivo, i+1+_sizeEconomica );
    }
}

Boeing747::Boeing747() : Avion() ,_descripcion("Este es un avion Boeing747") , _image("Avion::Boeing747")
{
    _dispEconomica = _sizeEconomica;
    _dispEjecutiva = _sizeEjecutiva;
    setAsientos();
}

Asiento*& Boeing747::getAsientos()
{
    return this->_asientos;
}

Boeing747& Boeing747::operator[](int a)
{
    return *(this+a);
}