#include "codigovuelo.h"

void CodigoVuelo::setCod(string cod)
{
    _cod = cod;
}
void CodigoVuelo::setDescripcion(string descripcion)
{
    _descripcion = descripcion;
}
void CodigoVuelo::setDestino(Destino destino)
{
    _destino = destino;
}
void CodigoVuelo::setLinea(Aerolinea linea)
{
    _linea = linea;
}
void CodigoVuelo::setOrigen(Origen origen)
{
    _origen = origen;
}
void CodigoVuelo::setSalida(Tiempo salida)
{
    _salida = salida;
}
void CodigoVuelo::setUnidad(Avion& unidad)
{
    _unidad = &unidad;
}
