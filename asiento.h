#ifndef ASIENTO_H
#define ASIENTO_H

enum Tipo { ejecutivo, economico};
class Asiento
{
    //int _width;
    //int _heigth;
    int _number;
    Tipo _location;
    bool _status;
public:
    Asiento(Tipo,int);
    //inline int getWidth() {return _width;}
    //inline int getHeight() {return _heigth;}
    inline int getNumber() {return _number;}
    inline Tipo getLocation() {return _location;}
    inline bool getStatus() {return _status;}
    void setNumber(int);
    void setLocation(Tipo);
    void setStatus(bool);
    Asiento& operator[](int);
    friend ostream& operator<< (ostream&, Asiento&);
};

#endif // ASIENTO_H
