#include "fecha.h"

const int Fecha::_dlength[13] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

bool Fecha::esBisiesto(int anio)
{
     return ((anio % 4 == 0 && anio % 100 != 0) || anio % 400 == 0);
}

bool Fecha::setDia(int dia)
{
    if(_mes == 2 && esBisiesto(_anio) == true)
    {
        if(dia > 0 && dia <= 29)
        {
            _dia = dia;
            return true;
        }
    }else
    {
        if(dia > 0 && dia < _dlength[_mes])
        {
            _dia = dia;
            return true;
        }
    }
    return false;
}
bool Fecha::setMes(int mes)
{
    if(mes > 0 && mes <= 12)
    {
        _mes = mes;
        return true;
    }
    return false;
}
bool Fecha::setAnio(int anio)
{
    if(anio > 1900 && anio <= 3000)
    {
        _anio = anio;
        return true;
    }
    return false;
}
bool Fecha::addAnios(int anios)
{
    if(anios > 0)
    {
        _anio += anios;
        return true;
    }
    return false;
}
bool Fecha::addMeses(int meses)
{
    if(meses > 0)
    {
        int a = meses/12;
        if(a == 0)
        {
            if(_mes + meses <= 12)
            {
                _mes += meses;
            }
            else
            {
                _mes = 1;
                addAnios(1);
            }
        }
        else
        {
            addAnios(a);
            _mes += (meses - (a*12));
        }
    }
    return false;
}
bool Fecha::addDias(int dias)
{
    int tdias = _dia + dias;
    int act = 0;
    if(dias > 0)
    {
        (_mes == 2)?( esBisiesto(_anio)?act = 29: act= 28):(act = _dlength[_mes]);
        while( tdias > act  )
        {
            tdias -= act;
            addMeses(1);
            (_mes == 2)?( esBisiesto(_anio)?act = 29: act= 28):(act = _dlength[_mes]);
        }
        _dia = tdias;
    }
    return false;
}
Fecha& Fecha::operator =(const Fecha& F)
{
    if(this != &F)
    {
        _dia = F._dia;
        _mes = F._mes;
        _anio = F._anio;
        return *this;
    }
    else
    {
        return *this;
    }
}
ostream & operator << ( ostream & output, Fecha & F )
{
    if(F._dia < 10)
    {
        output << "0" << F._dia << "/";
    }
    else
    {
        output << F._dia << "/";
    }
    if(F._mes < 10)
    {
        output << "0" << F._mes << "/";
    }
    else
    {
        output << F._mes << "/";
    }
    output << F._anio << endl;
    return output;
}
