#ifndef PASAJE_H
#define PASAJE_H
#include<codigovuelo.h>

class Pasaje
{
private:
    CodigoVuelo* _id;
    Fecha _date;
    bool _food;
    unsigned int _MaxLuggage;
    Asiento *_seat;
    Persona *_passenger;
public:
    Pasaje::Pasaje() : _id(NULL), _date(), _seat(NULL), _passenger(NULL), _food(false), _MaxLuggage(0){ }
    Pasaje(CodigoVuelo&, Fecha, bool, unsigned int, Asiento&, Persona&);
    ~Pasaje() { if(_seat != NULL) delete _seat; if(_passenger != NULL) delete _passenger; }
    inline CodigoVuelo* getCodigoVuelo() const { return _id; }
    inline Fecha getFecha() const { return _date; }
    inline bool tieneComida() const { return _food; }
    inline unsigned int getMaxLuggage() const { return _MaxLuggage; }
    inline Asiento* getAsiento() const { return _seat; }
    inline Persona* getPasajero() const {return _passenger; }
    void setCodigoVuelo(CodigoVuelo&);
    void setFecha(Fecha);
    void setComida(bool);
    void setMaxLuggage(unsigned int);
    void setAsiento(Asiento&);
    void setPasajero(Persona&);
    friend ostream& operatot<<(ostream&, Pasaje&);
};

#endif // PASAJE_H
