#ifndef CODIGOVUELO_H
#define CODIGOVUELO_H
#include <tiempo.h>
#include <avion.h>
enum Destino {Brazil, Peru, Colombia};
enum Origen {Venezuela};
enum Aerolinea {None,Laser,Conviasa};

class CodigoVuelo
{
private:
    string _cod;
    Tiempo _salida;
    string _descripcion;
    Origen _origen;
    Destino _destino;
    Aerolinea _linea;
    Avion* _unidad;

public:
    CodigoVuelo() : _salida(), _descripcion("Default") ,_linea(None) , _unidad(NULL){}
    CodigoVuelo(string c,Tiempo t,string d,Origen o,Destino de,Eerolinea a) : _cod(c), _descripcion(d), _salida(t), _origen(o), _destino(de), _linea(ae) {}
    ~CodigoVuelo() {if (_unidad != NULL)
                    delete _unidad;}
    void setOrigen(Origen);
    void setDestino(Destino);
    void setCod(string);
    void setSalida(Tiempo);
    void setDescripcion(string);
    void setLinea(Aerolinea);
    void setUnidad(Avion&);
    inline string getCod(){return _cod;}
    inline Tiempo getTiempo(){return _salida;}
    inline string getDescripcion(){return _descripcion;}
    inline Origen getOrigen(){return _origen;}
    inline Destino getDestino(){return _destino;}
    inline Aerolinea getAerolinea(){return _linea;}
    inline Avion& getAvion(){return *_unidad;}
};

#endif // CODIGOVUELO_H
