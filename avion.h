#ifndef AVION_H
#define AVION_H
#include<fecha.h>
#include<asiento.h>

class Avion
{
private:
    int _idRegistro;
    Fecha _comienzoOperacion;
    bool _status;
protected:
    Asiento* _asientos;
    virtual void setAsientos() = 0;
public:
    Avion(): _idRegistro(0), _comienzoOperacion() , _status(false){}
    Avion(Avion& A) : _idRegistro(A._idRegistro), _comienzoOperacion(A._comienzoOperacion) , _status(A._status){}
    Avion(int r, Fecha f, bool s) : _idRegistro(r), _comienzoOperacion(f), _status(s){}
    virtual ~Avion(){ delete[] _asientos;}
    inline int getRegistro(){return _idRegistro;}
    inline Fecha getFecha(){return _comienzoOperacion;}
    inline bool getStatus(){return _status;}
    void setId(int);
    void setOp(Fecha);
    void setStatus(bool);
    virtual string getDescripcion() = 0;
    virtual string getImage() = 0;
    virtual Asiento*& getAsientos() = 0;
    Avion& operator[](int);
};
#endif // AVION_H
