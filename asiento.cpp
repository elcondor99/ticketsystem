#include<asiento.h>
Asiento::Asiento(Tipo T, int n) : _location(T), _number(n), _status(false){}

void Asiento::setLocation(Tipo location)
{
    _location = location;
}
void Asiento::setNumber(int number)
{
    _number = number;
}
void Asiento::setStatus(bool status)
{
    _status = status;
}

Asiento& Asiento::operator [](int a)
{
    return *(this+a);
}

ostream& operator<< (ostream& output, Asiento& seat)
{
    output << seat->location == economico ? "Economico " : "Ejecutivo " << " " << seat->number << endl;
    return output;
}
