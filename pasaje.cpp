#include "pasaje.h"

Pasaje::Pasaje(CodigoVuelo& id, Fecha f, bool c, unsigned int l, Asiento& s, Persona& p)
{
    this->_id = &id;
    this->_date = f;
    this->_food = c;
    this->_MaxLuggage = l;
    this->_seat = &s;
    this->_passenger = &p;
    return;
}

void Pasaje::setCodigoVuelo(CodigoVuelo& id)
{
    this->_id = &id;
    return;
}

void Pasaje::setFecha(Fecha f)
{
    this->_date = f;
    return;
}

void Pasaje::setComida(bool c)
{
    this->_food = c;
    return;
}

void Pasaje::setMaxLuggage(unsigned int l)
{
    this->_MaxLuggage = l;
    return;
}

void Pasaje::setAsiento(Asiento& s)
{
    this->_seat = &s;
    return;
}

void Pasaje::setPasajero(Persona& p)
{
    this->_passenger = &p;
    return;
}

ostream& operatot<<(ostream & output, Pasaje & p)
{
    if(p->_passenger == NULL || p->_seat == NULL)
    {
        output << "Pasaje Invalido." << endl;
        return output;
    }
    output << "Vuelo: " << p->_id->getCod() << endl;
    output << *(p->_seat) << endl;
    output << p->_date << " " << p->_id->getTiempo() << endl; 
    output << *(p->_passenger) << endl;
    return output;
}