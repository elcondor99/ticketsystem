#ifndef BOEING747_H
#define BOEING747_H
#include<avion.h>
#include<asiento.h>

class Boeing747 : public Avion
{
private:
    static const int _sizeEconomica = 240;
    static const int _sizeEjecutiva = 60;
    int _dispEconomica;
    int _dispEjecutiva;
    string _descripcion;
    string _image;
    //Persona* _tripulacion
    void setAsientos();
public:
    Boeing747();
    Boeing747(int n, Fecha f, bool s) : Avion(n,f,s), _dispEconomica(_sizeEconomica), _dispEjecutiva(_sizeEjecutiva) {}
    ~Boeing747(){ /*delete _tripulacion;*/}
    string getDescripcion() {return _descripcion;}
    string getImage() {return _image;}
    Asiento*& getAsientos();
    Boeing747& operator[](int);
};

#endif // BOEING747_H
